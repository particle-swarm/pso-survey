## Copyright (c) 2014 Siddharth Ravikumar <sravik@bgsu.edu>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import cmath

from random import random
from sys import maxint

from components.oproblem import OProblem
from components.particle import Particle
from components.pso import PSOEngine


class Shubert(OProblem):
    """Shubert's function. Goal is to minimize it."""

    def __init__(self, dimensions):
        self.dimensions = dimensions
        self.pos_min = -10
        self.pos_max = 10


    def objective_function(self, d_variables):
        """Shubert's function is the objective function."""

        result = 0.0
        sum1 = 0.0
        sum2 = 0.0

        for x,y in d_variables:
            sum1 += 1j * cmath.cos(1j + (1j + 1) * x)
            sum2 += 1j * cmath.cos(1j + (1j + 1) * y)

        result = sum1 * sum2

        return result


    def gen_rand_value(self):
        """Generates a random tuple within the range."""

        x = super(Shubert,self).gen_rand_value()
        y = super(Shubert,self).gen_rand_value()

        return (x,y)


class ShubertParticle(Particle):
    """A Shubert Particle"""

    # since it is a minimization problem, we set the local_best to a
    # large positive value.
    local_best_value = maxint


    def __init__(self, args=[7]):
        """Initiialize the particle."""
        # init. particle's dimensions
        self.dimensions = args[0]

        # create shubert object.
        self.objc = Shubert(self.dimensions)

        # initialzes d_variables and sets velocity limits
        super(ShubertParticle, self).__init__(args)


    def update_velocity(self, gbest_particle):
        """Updates the velocity of the particle."""

        gbest_dvs = gbest_particle.d_variables

        # update velocity
        self.velocity = self.velocity*self.w + (self.cog_c * random()\
                           * (self.local_best_pos - self.d_variables))\
                           + (self.social_c * random()\
                           * (gbest_dvs - self.d_variables))

        for i in range(self.dimensions):
            if self.velocity[i][0] < self.vel_min:
                self.velocity[i][0] = self.vel_min
            elif self.velocity[i][0] > self.vel_max:
                self.velocity[i][0] = self.vel_max

            if self.velocity[i][1] < self.vel_min:
                self.velocity[i][1] = self.vel_min
            elif self.velocity[i][1] > self.vel_max:
                self.velocity[i][0] = self.vel_max


    def update_position(self):
        """Updates particle's position."""

        for i in range(self.dimensions):
            self.d_variables[i] += self.velocity[i]

            if self.d_variables[i][0] < self.objc.pos_min:
                self.d_variables[i][0] = self.objc.pos_min
            elif self.d_variables[i][0] > self.objc.pos_max:
                self.d_variables[i][0] = self.objc.pos_max

            if self.d_variables[i][1] < self.objc.pos_min:
                self.d_variables[i][1] = self.objc.pos_min
            elif self.d_variables[i][1] > self.objc.pos_max:
                self.d_variables[i][1] = self.objc.pos_max


    def fitness_function(self):
        """Calculates the fitness value of particle at present position."""

        self.fitness_value = self.objc.objective_function(self.d_variables)

        if self.fitness_value.real < self.local_best_value.real:
            self.local_best_value = self.fitness_value
            self.local_best_pos = self.d_variables

        return self.fitness_value


    def better_than(self, particle):
        """Returns true if this particle has a better fitness value than particle's local best.

        """

        if self.fitness_value.real < particle.local_best_value.real:
            return True

        return False

    def __repr__(self):
        return "Shubert"


if __name__ == "__main__":
    engine = PSOEngine(100, ShubertParticle, [7])
    engine.run_swarm()
    engine.print_results()
