## Copyright (c) 2014 Siddharth Ravikumar <sravik@bgsu.edu>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


from math import pow
from random import random
from sys import maxint

from components.oproblem import OProblem
from components.particle import Particle
from components.pso import PSOEngine


class SixHump(OProblem):
    """Six-hump camel back function. Goal is to minimize it."""


    def __init__(self, dimensions):
        # the dimensions are fixed for this function.
        self.dimensions = 2
        self.genX = True

    def objective_function(self, d_variables):
        """Six-hump camel back function is the objective function."""

        x = d_variables[0]
        y = d_variables[1]

        result = (4 - (2.1 * pow(x, 2)) + ((1.0/3.0) * pow(x, 4))) * pow(x, 2)
        result += x * y
        result += 4 * (pow(y, 2) - 1) * pow(y, 2)

        return result


    def gen_rand_value(self):
        """Generates a random value between -3 & 3 for `x' and -2 & 2 for `y'.
        """

        if self.genX:
            value =  (3 + 3) * random() - 3
            self.genX = False
        else:
            value =  (2 + 2) * random() - 2

        return value


class SixHumpParticle(Particle):
    """A Six Hump camel back particle"""

    # decision variables (list)
    x = None
    y = None

    # since it is a minimization problem, we set the local_best to a
    # large positive value.
    local_best_value = maxint


    def __init__(self, args=[2]):
        """Initiialize the particle."""
        # particle has only 2 dimensions.
        self.dimensions = 0
        # create sixhump object.
        self.sixhump = SixHump(self.dimensions)

        # initialzes d_variables and sets velocity limits
        self.x, self.y = self.objc.gen_values()

        # particle's velocity - lower and upper limit.
        self.vel_min = 0.25 * self.objc.pos_min
        self.vel_max = 0.25 * self.objc.pos_max

    def update_position(self):
        """Updates the particle's position."""
        self.x, self.y = self.sixhump.gen_values()


    def fitness_function(self):
        """Calculates the fitness value of particle at present position."""

        self.fitness_value = self.sixhump.objective_function(self.x, self.y)

        if self.fitness_value < self.local_best:
            self.local_best = self.fitness_value

        return self.fitness_value


    def better_than(self, particle):
        """Returns true if this particle has a better fitness value than particle's local best.

        """
        # if particle's local_best is None
        if not particle.local_best:
            return True

        if self.fitness_value < particle.local_best:
            return True

        return False

    def __repr__(self):
        return "Six Hump"


if __name__ == "__main__":
    engine = PSOEngine(1000, SixHumpParticle, [2])
    engine.run_swarm()
