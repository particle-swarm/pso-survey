## Copyright (c) 2014 Siddharth Ravikumar <sravik@bgsu.edu>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import math

from random import random
from sys import maxint

from components.oproblem import OProblem
from components.particle import Particle
from components.pso import PSOEngine


class Rastrigin(OProblem):
    """Rastrigin's function. Goal is to minimize it."""


    def __init__(self, dimensions):
        self.dimensions = dimensions
        self.pos_min = -5.12
        self.pos_max = 5.12

    def objective_function(self, d_variables):
        """Rastrigin's function is the objective function."""

        ## The minimum is 420.0 @ (0, 0, 0,..)

        result = 0.0
        n = self.dimensions

        for x in d_variables:
            result += 10*n + ((x*x) - (10 * math.cos(2 * math.pi * x)))

        return result


class RastriginParticle(Particle):
    """A Rastrigin Particle"""

    # since it is a minimization problem, we set the local_best to a
    # large positive value.
    local_best_value = maxint


    def __init__(self, args=[7]):
        """Initiialize the particle."""

        # init. particle's dimensions
        self.dimensions = args[0]

        # create rastrigin object.
        self.objc = Rastrigin(self.dimensions)

        # initialzes d_variables and sets velocity limits
        super(RastriginParticle, self).__init__(args)


    def fitness_function(self):
        """Calculates the fitness value of particle at present position."""

        self.fitness_value = self.objc.objective_function(self.d_variables)

        if self.fitness_value < self.local_best_value:
            self.local_best_value = self.fitness_value
            self.local_best_pos = self.d_variables

        return self.fitness_value


    def better_than(self, particle):
        """Returns true if this particle has a better fitness value than particle's local best.

        """

        if self.fitness_value < particle.local_best_value:
            return True

        return False


    def __repr__(self):
        return "Rastrigin"


if __name__ == "__main__":
    engine = PSOEngine(100, RastriginParticle, [7])
    engine.run_swarm()
    engine.print_results()
