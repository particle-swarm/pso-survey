## Copyright (c) 2014 Siddharth Ravikumar <sravik@bgsu.edu>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import ackley
import dejong
import griewank
import rastrigin
import rosenbrock
import schwefel
import shubert
import sixhump
import xinsheyang
import zakharov

from components.pso import benchmark_pso

print "Benchmarking Ackley..."
benchmark_pso(ackley.AckleyParticle)
print "Benchmarking Dejong..."
benchmark_pso(dejong.DeJongParticle)
print "Benchmarking Griewank..."
benchmark_pso(griewank.GriewankParticle)
print "Benchmarking Rastringin..."
benchmark_pso(rastrigin.RastriginParticle)
print "Benchmarking Rosenbrock..."
benchmark_pso(rosenbrock.RosenbrockParticle)
print "Benchmarking Schwefel..."
benchmark_pso(schwefel.SchwefelParticle)
print "Benchmarking Shubert..."
benchmark_pso(shubert.ShubertParticle)
#print "Benchmarking SixHump..."
#benchmark_pso(sixhump.SixHumpParticle) # 2-dimensions.
print "Benchmarking XinSheYang..."
benchmark_pso(xinsheyang.XinSheYangParticle)
print "Benchmarking Zakharov..."
benchmark_pso(zakharov.ZakharovParticle)
