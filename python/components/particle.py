## Copyright (c) 2014 Siddharth Ravikumar <sravik@bgsu.edu>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from random import random

class Particle(object):
    """Particle class."""

    dimensions = None

    social_c = 2.0 # social coefficient
    cog_c = 2.0 # cognitive coefficient
    w = 0.65 # inertial constant
    w_min = 0.40
    w_max = 0.90

    # particle's velocity
    velocity = 0.0

    # particle's velocity - lower and upper limit. Must be initialized
    # by subclass.
    vel_min = None
    vel_max = None

    # current solution of the particle.
    fitness_value = None
    # best solution found by the particle so far.
    local_best_value = None
    # local best position
    local_best_pos = None

    # Objective function/problem class. Must be initialized by
    # subclass.
    objc = None

    # numpy array of decision variables. Must be initialized by
    # subclass.
    d_variables = None

    def __init__(self, args=[]):
        """Initializes particle d_variables and velocity limits.

        The subclass must override this and initialize the `dimensions'
        and `objc' and call this using super().

        args: List containing paraphernalia for initializing the
        particle.

        """

        # set initial values for decision variables.
        self.d_variables = self.objc.gen_values()

        # particle's velocity - lower and upper limit.
        self.vel_min = 0.25 * self.objc.pos_min
        self.vel_max = 0.25 * self.objc.pos_max

        # velocity change counter
        self.velc_count = 0

    def update_velocity(self, gbest_particle):
        """Updates the velocity of the particle."""

        gbest_dvs = gbest_particle.d_variables


        if self.velc_count > 250:
            self.w = (self.w_max - self.w_min) * random() + self.w_min
            self.velc_count = 0

        self.velc_count += 1

        # update velocity
        self.velocity = self.velocity*self.w + (self.cog_c * random()\
                           * (self.local_best_pos - self.d_variables))\
                           + (self.social_c * random()\
                           * (gbest_dvs - self.d_variables))

        for i in range(self.dimensions):
            if self.velocity[i] < self.vel_min:
                self.velocity[i] = self.vel_min
            elif self.velocity[i] > self.vel_max:
                self.velocity[i] = self.vel_max

    
    def update_position(self):
        """Updates particle's position."""

        for i in range(self.dimensions):
            self.d_variables[i] += self.velocity[i]

            if self.d_variables[i] < self.objc.pos_min:
                self.d_variables[i] = self.objc.pos_min
            elif self.d_variables[i] > self.objc.pos_max:
                self.d_variables[i] = self.objc.pos_max


    def fitness_function(self):
        """Call particle's fitness function. Needs to be fleshed out in subclass."""
        pass


    def better_than(self, particle):
        """Returns true if this particle has a better fitness value than `particle'.

        Needs to implemented by subclass.
        """
        return False

    def __repr__(self):
        return "Particle's best is %d" % self.local_best
