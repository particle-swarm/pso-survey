## Copyright (c) 2014 Siddharth Ravikumar <sravik@bgsu.edu>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import copy
import Gnuplot

#from numpy import mean, array
from time import clock

from particle import Particle

class PSOEngine(object):
    """The PSO Engine."""

    def __init__(self, swarm_size, particle_class, particle_args=None):
        """Initialize the engine.

        particle_class: Canonical name of the Particle class.

        particle_args: List of arguments for initializing the Particle
        class.

        """

        # contains the best solution.
        self.global_best = None
        # Points to the particle with best solution.
        self.best_particle = None # set in init_particles()
        self.particles = []
        # Keeps count of the times global_best hasn't changed.
        self.gb_not_changed = 0

        self.particle_class = particle_class
        self.particle_args = particle_args
        self.swarm_size = swarm_size
        # Defines the limit of the no. of consecutive iterations where
        # global best is not changed.
        self.gb_not_changed_limit = particle_class().dimensions * 90000
        self.init_particles()



    def init_particles(self):
        """Initializes the swarm particles."""

        for i in range(self.swarm_size):
            # create a particle
            particle = self.particle_class(self.particle_args)

            if i == 0:
                self.best_particle = particle
                self.global_best = self.best_particle.fitness_function()

            self.particles.append(particle)

    def run_swarm(self):
        """Runs the PSO Algorithm."""

        while self.gb_not_changed <= self.gb_not_changed_limit:

            for particle in self.particles:
                fitness_value = particle.fitness_function()

                if particle.better_than(self.best_particle):
                    self.global_best = fitness_value
                    self.best_particle = copy.deepcopy(particle)
                else:
                    self.gb_not_changed += 1

                particle.update_velocity(self.best_particle)
                particle.update_position()


        return self.global_best, self.best_particle


    def print_results(self):
        """Prints the results found by the PSO Algorithm."""

        print "Optimal solution: %r" % self.global_best
        print self.best_particle.local_best_pos



def benchmark_pso(particle_class, particle_args=[2],
                  dimensions=[10, 20, 30, 40, 50, 60, 70, 80, 90, 100]):
    """Runs the Swarm 10 times and calculates average execution time and fitness value.

    particle_class: The particle to benchmark.
    args: The args that has to be passed to the particle_class to initiate it.
    dimensions: list of dimensions for which the particle needs to be benchmarked.

    """

    # contains list of avg. fitness values for different dimensions.
    dim_avg_fitness = {}
    # contains list of avg. time for different dimensions.
    dim_avg_time = {}

    iter_fitness_values = []
    iter_time_values = []

    data_fitness = 'data_fitness.txt'
    data_time = 'data_time.txt'
    dataf = open(data_fitness, 'w')
    datat = open(data_time, 'w')

    for i in range(10):

        fitness_values = []
        time_values = []

        avg_fitness = 0
        avg_time = 0

        for d in range(len(dimensions)):
            dimension = dimensions[d:d+1]
            start = clock()
            engine = PSOEngine(100, particle_class, dimension)
            best_fitness, best_particle = engine.run_swarm()
            time_taken = clock() - start # in seconds

            if not isinstance(best_fitness, complex):
                dataf.write("%f " % best_fitness)
            else:
                dataf.write("%f " % best_fitness.real)

            datat.write("%f " % time_taken)
            fitness_values.append(best_fitness)
            time_values.append(time_taken)

        # dim_avg_fitness[dimension[0]] = mean(fitness_values)
        # dim_avg_time[dimension[0]] = mean(time_values)
        dataf.write("\n")
        datat.write("\n")
        iter_fitness_values.append(fitness_values)
        iter_time_values.append(time_values)

    dataf.close()
    datat.close()
    print iter_fitness_values
    print iter_time_values

    # keys = dimensions
    # keys.sort()
    # avg_fitness_values = []
    # avg_time_values = []

    # for key in keys:
    #     avg_fitness_values.append(dim_avg_fitness.get(key))
    #     avg_time_values.append(dim_avg_time.get(key))

    # avg_fitness_x = array(keys)
    # avg_fitness_y = array(avg_fitness_values)

    # avg_time_x = array(keys)
    # avg_time_y = array(avg_time_values)

    fitness_graph_title = "Test Problem - %s  - Dimensions vs. Fitness" % particle_class()
    g_fitness = Gnuplot.Gnuplot(persist=1)
    g_fitness("set title '%s'" % fitness_graph_title)
    g_fitness.xlabel("Dimensions")
    g_fitness.ylabel("Fitness")
    g_fitness("set style fill solid 0.25 border -1")
    g_fitness("set style boxplot")
    g_fitness("set style data boxplot")
    g_fitness("set boxwidth 5.0")
    g_fitness("plot for [i=10:100:10] '%s' using (i):i/10 notitle" % data_fitness)

    time_graph_title = "Test Problem - %s  - Dimensions vs. Time" % particle_class()
    g_time = Gnuplot.Gnuplot(persist=1)
    g_time("set title '%s'" % time_graph_title)
    g_time.xlabel("Dimensions")
    g_time.ylabel("Time")
    g_time("set style fill solid 0.25 border -1")
    g_time("set style boxplot")
    g_time("set style data boxplot")
    g_fitness("set boxwidth 5.0")
    g_time("plot for [i=10:100:10] '%s' using (i):i/10 notitle" % data_time)
