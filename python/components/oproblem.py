## Copyright (c) 2014 Siddharth Ravikumar <sravik@bgsu.edu>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from numpy import array
from random import random


class OProblem(object):
    """Optimizition Problem class."""

    # Defined by the particle.
    dimensions = None

    # Lower and upper bounds of the 
    pos_min = None
    pos_max = None



    def objective_function(self, d_variables):
        """Optimization Problem's objective function. Must be fleshed out in the subclass"""
        pass


    def gen_rand_value(self):
        """Generates a random value within the range."""

        return (self.pos_max - self.pos_min) * random() + self.pos_min

    def gen_values(self):
        """Generates a random value for the decision variables"""
        d_variables = array([self.gen_rand_value() for i in range(self.dimensions)])

        return d_variables
