## Copyright (c) 2014 Siddharth Ravikumar <sravik@bgsu.edu>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from random import randint

from components.particle import Particle
from components.pso import PSOEngine

class ToyShop(object):
    """The Toy Shop profit maximization problem."""

    def calculate_profit(self, soldiers, trains):
        """Calculate profit given the no. of soldiers & trains."""

        profit = (3 * soldiers) + (2 * trains)
        return profit

    def gen_values(self):
        """Generates a random value for the no. of soldiers & trains.

        The random values satisfy the constraints of the ToyShop problem.
        """

        while True:
            soldiers = randint(0, 40)
            trains = randint(0, 80)
            
            # see if the above random values satisfy the constraint of the
            # hours of finishing & carpentry.
        
            # check finishing hours constraint.
            if ((2 * soldiers) + trains) > 100:
                # go back to beginning of `while' to gen. values again.
                continue

            # check carpentry hours constraint.
            if (soldiers + trains) > 80:
                # go back to beginning of `while' to gen. values again
                continue

            # If here. It means we found the right value. So return
            # them.
            return soldiers, trains


class ToyShopParticle(Particle):
    """Toy shop particle for the ToyShop problem."""

    dimensions = 2

    toy_shop = ToyShop()
    
    # These are the decision variables.
    soldiers = None
    trains = None
    
    def __init__(self, args=[]):
        self.fitness_value = 0
        self.local_best = 0

    def __repr__(self):
        return "ToyShopParticle - Soldiers: %d - Trains: %d" % \
            (self.soldiers, self.trains)


    def update_position(self):
        """Updates the value of soldiers & trains.

        Returns the new fitness value of the particle.
        """

        self.soldiers, self.trains = self.toy_shop.gen_values()


    def fitness_function(self):
        """Gets the profit for the present values of soldiers & trains.

        The profit is the fitness value. This value is returned.
        """

        profit = self.toy_shop.calculate_profit(self.soldiers, self.trains)

        self.fitness_value = profit

        if self.fitness_value > self.local_best:
            self.local_best = self.fitness_value

        return self.fitness_value


    def better_than(self, particle):
        """Returns true if this particle has a better fitness value than particle's local best.

        """

        # if particle's local_best is None.
        if not particle.local_best:
            return True

        if self.fitness_value > particle.local_best:
            return True

        return False


if __name__ == "__main__":
    engine = PSOEngine(1500, ToyShopParticle, [2])
    engine.run_swarm()
    engine.print_results()
