#!/bin/bash

# for svg files under */ 
for i in $(ls ./*.svg); do
	file=$(basename $i)
	filename="${file%.*}"
	eps_file="eps/$filename.eps"
	echo "writing...$eps_file"
    inkscape -z -D --file=$file --export-eps=$eps_file
done
